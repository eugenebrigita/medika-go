from django.urls import path
from . import views

urlpatterns = [
    path('buat-layanan-poliklinik/', views.buat_layanan_poliklinik, name="buat-layanan-poliklinik"),
    path('daftar-layanan-poliklinik/', views.daftar_layanan_poliklinik, name="daftar-layanan-poliklinik"),
    path('update-layanan-poliklinik/', views.update_layanan_poliklinik, name="update-layanan-poliklinik"),
    path('delete-layanan-poliklinik/', views.delete_layanan_poliklinik, name="delete-layanan-poliklinik"),
    path('daftar-jadwal-poliklinik/', views.daftar_jadwal_poliklinik, name="daftar-jadwal-poliklinik"),
    path('update-jadwal-poliklinik/', views.update_jadwal_poliklinik, name="update-jadwal-poliklinik"),
    path('delete-jadwal-poliklinik/', views.delete_jadwal_poliklinik, name="delete-jadwal-poliklinik"),
]