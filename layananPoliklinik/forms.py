from django import forms

Kode_rs_list = (
    (1,'RS01'),
	(2,'RS02'),
	(3,'RS03'),
	(4,'RS04'),
	(5,'RS05')
	
)
Id_Dokter_list = (
    (1,'D01'),
	(2,'D02'),
	(3,'D03'),
	(4,'D04'),
	(5,'D05'),
	(6,'D06'),
	(7,'D07')
		
)

class PembuatanLayananPoliklinikForm(forms.Form):
    nama_attrs = {
        'class': 'form-control',
        'placeholder': 'Jadwal Poliklinik',
    }

    nama_layanan = forms.CharField(
        label='Nama Layanan',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    deskripsi = forms.CharField(
        label='Deskripsi',
        required=True,
        max_length=9999,
        widget=forms.TextInput())

    kode_rs = forms.CharField(
        label='Kode RS',
        required=True,
        max_length=30,
        widget=forms.TextInput())
    
    daftar_jadwal = forms.CharField(
        label='Daftar Jadwal',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=nama_attrs))
    
    hari = forms.CharField(
        label='Hari',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    waktu_mulai =(forms.TimeField(label='Waktu Mulai'))

    waktu_selesai =(forms.TimeField( label='Waktu Selesai'))

    kapasistas = forms.CharField(
        label='Kapasitas',
        required=True,
        max_length=30,
        widget=forms.TextInput())



class UpdateLayananPoliklinik(forms.Form):

    id_poliklinik = forms.CharField(
        label='ID Poliklinik',
        required=True,
        max_length=30,
        widget=forms.TextInput())
    

    nama_layanan = forms.CharField(
        label='Nama Layanan',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    deskripsi = forms.CharField(
        label='Deskripsi',
        required=False,
        max_length=9999,
        widget=forms.TextInput())

    Kode_Rs = forms.ChoiceField(label='Kode RS', choices=Kode_rs_list)





# class PembuatanJadwalForm(forms.Form):
#     kode_rs_cabang_attrs = {
#         'class': 'form-control',
#         'placeholder': 'RS01',
#     }

#     nama_attrs = {
#         'class': 'form-control',
#         'placeholder': 'American Health',
#     }

#     tanggal_pendirian_attrs = {
#         'class': 'form-control',
#         'placeholder': '09 September 1992',
#     }

#     jalan_attrs = {
#         'class': 'form-control',
#         'placeholder': 'Lyons',
#     }

#     nomor_attrs = {
#         'class': 'form-control',
#         'placeholder': '20910',
#     }

#     kota_attrs = {
#         'class': 'form-control',
#         'placeholder': 'Arizona',
#     }

#     kode_rs_cabang = forms.CharField(
#         label='Kode RS',
#         required=True,
#         max_length=30,
#         widget=forms.TextInput(attrs=kode_rs_cabang_attrs))

#     nama = forms.CharField(
#         label='Nama',
#         required=True,
#         max_length=30,
#         widget=forms.TextInput(attrs=nama_attrs))

#     tanggal_pendirian = forms.CharField(
#         label='Tanggal Pendirian',
#         required=True,
#         max_length=30,
#         widget=forms.TextInput(attrs=tanggal_pendirian_attrs))

#     jalan = forms.CharField(
#         label='Jalan',
#         required=False,
#         max_length=30,
#         widget=forms.TextInput(attrs=jalan_attrs))

#     nomor = forms.CharField(
#         label='Nomor',
#         required=False,
#         max_length=30,
#         widget=forms.TextInput(attrs=nomor_attrs))

#     kota = forms.CharField(
#         label='Kota',
#         required=True,
#         max_length=30,
#         widget=forms.TextInput(attrs=kota_attrs))


class UpdateJadwalPoliklinik(forms.Form):
    
    id_jadwal = forms.CharField(
        label='ID Jadwal Layanan Poliklinik',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    hari = forms.CharField(
        label='Hari',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    waktu_mulai =(forms.TimeField(label='Waktu Mulai'))

    waktu_selesai =(forms.TimeField( label='Waktu Selesai'))

    kapasistas = forms.CharField(
        label='Kapasitas',
        required=True,
        max_length=30,
        widget=forms.TextInput())

    Id_Dokter = forms.ChoiceField(label='ID Dokter', choices=Id_Dokter_list)

    id_poliklinik = forms.CharField(
        label='ID Poliklinik',
        required=True,
        max_length=30,
        widget=forms.TextInput())