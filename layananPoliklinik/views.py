from django.shortcuts import render, redirect
from .forms import *

# Create your views here.
data_layanan_poliklinik = [
    {
        'id_poliklinik' : "POL01",
        'kode_rs': "RS01",
        'nama' : "Poliklinik Anak" ,
        'Deskripsi' : "",
    },

    {
        'id_poliklinik' : "POL02",
        'kode_rs': "RS02",
        'nama' : "Poliklinik Gigi" ,
        'Deskripsi' : "",
    },

    {
        'id_poliklinik' : "POL03",
        'kode_rs': "RS03",
        'nama' : "Poliklinik Mata" ,
        'Deskripsi' : "",
    },

    {
        'id_poliklinik' : "POL04",
        'kode_rs': "RS04",
        'nama' : "Poliklinik Telinga Hidung Tenggorokan" ,
        'Deskripsi' : "",
    },

    {
        'id_poliklinik' : "POL05",
        'kode_rs': "RS05",
        'nama' : "Poliklinik Bedah" ,
        'Deskripsi' : "",
    },

]

data_jadwal_poliklinik = [

    {
        'id_jadwal_poliklinik' : "JP01",
        'waktu_mulai' : "12:00:00",
        'waktu_selesai' : "14:00:00",
        'hari' : "Rabu",
        'kapasitas' : "",
        'id_dokter' : "D01",
        'id_poliklinik' : "POL02",
    },
    {
        'id_jadwal_poliklinik' : "JPO02",
        'waktu_mulai' : "08:00:00",
        'waktu_selesai' : "12:00:00",
        'hari' : "Senin",
        'kapasitas' : "8",
        'id_dokter' : "D04",
        'id_poliklinik' : "POL02",
    },
    {
        'id_jadwal_poliklinik' : "JPO03",
        'waktu_mulai' : "10:00:00",
        'waktu_selesai' : "12:00:00",
        'hari' : "Jumat",
        'kapasitas' : "5",
        'id_dokter' : "D05",
        'id_poliklinik' : "POL03",
    },
    {
        'id_jadwal_poliklinik' : "JPO04",
        'waktu_mulai' : "13:00:00",
        'waktu_selesai' : "16:00:00",
        'hari' : "Selasa",
        'kapasitas' : "7",
        'id_dokter' : "D06",
        'id_poliklinik' : "POL04",
    },
    {
        'id_jadwal_poliklinik' : "JPO05",
        'waktu_mulai' : "13:00:00",
        'waktu_selesai' : "18:00:00",
        'hari' : "Jumat",
        'kapasitas' : "4",
        'id_dokter' : "D03",
        'id_poliklinik' : "POL05",
    },
    {
        'id_jadwal_poliklinik' : "JPO06",
        'waktu_mulai' : "14:00:00",
        'waktu_selesai' : "17:00:00",
        'hari' : "Kamis",
        'kapasitas' : "5",
        'id_dokter' : "D02",
        'id_poliklinik' : "POL01",
    },
    {
        'id_jadwal_poliklinik' : "JPO07",
        'waktu_mulai' : "08:00:00",
        'waktu_selesai' : "12:00:00",
        'hari' : "Selasa",
        'kapasitas' : "6",
        'id_dokter' : "D07",
        'id_poliklinik' : "POL02",
    },
    {
        'id_jadwal_poliklinik' : "JPO08",
        'waktu_mulai' : "08:00:00",
        'waktu_selesai' : "12:00:00",
        'hari' : "Rabu",
        'kapasitas' : "4",
        'id_dokter' : "D05",
        'id_poliklinik' : "POL03",
    },
    {
        'id_jadwal_poliklinik' : "JPO09",
        'waktu_mulai' : "17:00:00",
        'waktu_selesai' : "19:00:00",
        'hari' : "Senin",
        'kapasitas' : "4",
        'id_dokter' : "D06",
        'id_poliklinik' : "POL04",
    },
    {
        'id_jadwal_poliklinik' : "JPO10",
        'waktu_mulai' : "08:00:00",
        'waktu_selesai' : "13:00:00",
        'hari' : "Sabtu",
        'kapasitas' : "6",
        'id_dokter' : "D03",
        'id_poliklinik' : "POL05",
    },


]

def buat_layanan_poliklinik(request):
    create_layanan_poliklinik_form = PembuatanLayananPoliklinikForm(request.POST)
    if request.method == 'POST':
        return render(request, 'create_layanan_poliklinik.html')

    context = {
        'create_layanan_poliklinik_form': create_layanan_poliklinik_form,
    }

    return render(request, 'create_layanan_poliklinik.html', context)


def daftar_layanan_poliklinik(request):
    context = {'daftar_layanan_poliklinik': data_layanan_poliklinik}
    return render(request, 'daftar_layanan_poliklinik.html', context)

def daftar_jadwal_poliklinik(request):
    context = {'daftar_jadwal_poliklinik': data_jadwal_poliklinik}
    return render(request, 'daftar_jadwal_poliklinik.html', context)


def update_layanan_poliklinik(request):
    if request.method == "POST":
        return redirect('update-layanan-poliklinik')
    update_form = UpdateLayananPoliklinik()

    context = {
        'update_form': update_form,
    }

    return render(request, 'update_layanan_poliklinik.html', context)

def update_jadwal_poliklinik(request):
    if request.method == "POST":
        return redirect('update-jadwal-poliklinik')
    update_form = UpdateJadwalPoliklinik()

    context = {
        'update_form': update_form,
    }

    return render(request, 'update_jadwal_poliklinik.html', context)


def delete_layanan_poliklinik(request):
    return redirect('daftar-layanan-poliklinik')

def delete_jadwal_poliklinik(request):
    return redirect('daftar-jadwal-poliklinik')