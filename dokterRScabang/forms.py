from django import forms

Id_Dokter_list = (
    (1,'D01'),
	(2,'D02'),
	(3,'D03'),
	(4,'D04'),
	(5,'D05'),
	(6,'D06'),
	(7,'D07')
		
)

Kode_rs_list = (
    (1,'RS01'),
	(2,'RS02'),
	(3,'RS03'),
	(4,'RS04'),
	(5,'RS05')
	
)
class DaftarkanDokterRSCabangForm(forms.Form):
    Id_Dokter = forms.ChoiceField(label='ID Dokter', choices=Id_Dokter_list)
    Kode_Rs = forms.ChoiceField(label='Kode RS', choices=Kode_rs_list)

class UpdateDokterRSCabang(forms.Form):
    Id_Dokter = forms.ChoiceField(label='ID Dokter', choices=Id_Dokter_list)
    Kode_Rs = forms.ChoiceField(label='Kode RS', choices=Kode_rs_list)