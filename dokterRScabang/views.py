from django.shortcuts import render, redirect
from .forms import *


# Create your views here.
data_dokter_rs_cabang = [
    {
        'id_dokter' : "D01",
        'kode_rs': "RS01"
    },
    {
        'id_dokter' : "D01",
        'kode_rs': "RS02"
    },
    {
        'id_dokter' : "D02",
        'kode_rs': "RS02"
    },
    {
        'id_dokter' : "D02",
        'kode_rs': "RS05"
    },
    {
        'id_dokter' : "D03",
        'kode_rs': "RS03"
    },
    {
        'id_dokter' : "D04",
        'kode_rs': "RS04"
    },
    {
        'id_dokter' : "D04",
        'kode_rs': "RS01"
    },
    {
        'id_dokter' : "D05",
        'kode_rs': "RS05"
    },
    {
        'id_dokter' : "D06",
        'kode_rs': "RS01"
    },
    {
        'id_dokter' : "D07",
        'kode_rs': "RS03"
    },
]

def daftarkan_dokter_rs_cabang(request):
    regist_dokter_rs_cabang_form = DaftarkanDokterRSCabangForm(request.POST)
    if request.method == 'POST':
        return render(request, 'regist_dokter_rs_cabang.html')

    context = {
        'regist_dokter_rs_cabang_form': regist_dokter_rs_cabang_form,
    }

    return render(request, 'regist_dokter_rs_cabang.html', context)


def daftar_dokter_rs_cabang(request):
    context = {'daftar_dokter_rs_cabang': data_dokter_rs_cabang}
    return render(request, 'daftar_dokter_rs_cabang.html', context)


def update_dokter_rs_cabang(request):
    if request.method == "POST":
        return redirect('daftar-dokter-rs-cabang')
    update_form = UpdateDokterRSCabang()

    context = {
        'update_form': update_form,
    }

    return render(request, 'update_dokter_rs_cabang.html', context)


def delete_dokter_rs_cabang(request):
    return redirect('daftar-dokter-rs-cabang')