from django.urls import path
from . import views

urlpatterns = [
    path('daftarkan-dokter-rs-cabang/', views.daftarkan_dokter_rs_cabang, name="daftarkan-dokter-rs-cabang"),
    path('daftar-dokter-rs-cabang/', views.daftar_dokter_rs_cabang, name="daftar-dokter-rs-cabang"),
    path('update-dokter-rs-cabang/', views.update_dokter_rs_cabang, name="update-dokter-rs-cabang"),
    path('delete-dokter-rs-cabang/', views.delete_dokter_rs_cabang, name="delete-dokter-rs-cabang"),
    
]