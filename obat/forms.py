from django import forms
from django.forms import widgets
from .models import *


class CreateObatForm(forms.ModelForm):
    kode = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"kode",
    }))
    stok = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"stok",
    }))
    harga = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"harga",
    }))
    komposisi = forms.CharField(required=False, widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : False,
        "placeholder":"komposisi",
    }))
    bentuk_sediaan = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"bentuk_sediaan",
    }))
    merk_dagang = forms.CharField(widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"merk_dagang",
    }))
    class Meta:
                model = Obat                
                fields = ["kode", "stok", "harga", "komposisi", "bentuk_sediaan","merk_dagang"]

class UpdateObatForm(forms.ModelForm):

    kode = forms.CharField(widget=forms.HiddenInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"kode",
    }))
    stok = forms.CharField(required=False,widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"stok",
    }))
    harga = forms.CharField(required=False,widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"harga",
    }))
    komposisi = forms.CharField(required=False,widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : False,
        "placeholder":"komposisi",
    }))
    bentuk_sediaan = forms.CharField(required=False,widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"bentuk_sediaan",
    }))
    merk_dagang = forms.CharField(required=False,widget=forms.TextInput(attrs={
        "class" : "charfield",
        "required" : True,
        "placeholder":"merk_dagang",
    }))
    class Meta:
        model = Obat            
        fields = ["kode", "stok", "harga", "komposisi", "bentuk_sediaan","merk_dagang"]