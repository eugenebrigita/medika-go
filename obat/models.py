# This is an auto-generated Django model module.
# ```python manage.py inspectdb > models.py```
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Administrator(models.Model):
    nomor_pegawai = models.CharField(primary_key=True, max_length=50)
    username = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='username')
    kode_rs = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs')

    class Meta:
        managed = False
        db_table = 'administrator'


class AlergiPasien(models.Model):
    no_rekam_medis = models.OneToOneField('Pasien', models.DO_NOTHING, db_column='no_rekam_medis', primary_key=True)
    alergi = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'alergi_pasien'
        unique_together = (('no_rekam_medis', 'alergi'),)


class Asuransi(models.Model):
    nama = models.CharField(primary_key=True, max_length=50)
    nomor_telepon = models.CharField(max_length=20)

    class Meta:
        managed = False
        db_table = 'asuransi'


class AsuransiRsCabang(models.Model):
    nama_asuransi = models.OneToOneField(Asuransi, models.DO_NOTHING, db_column='nama_asuransi', primary_key=True)
    kode_rs = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs')

    class Meta:
        managed = False
        db_table = 'asuransi_rs_cabang'
        unique_together = (('nama_asuransi', 'kode_rs'),)


class DaftarObat(models.Model):
    no_resep = models.OneToOneField('Resep', models.DO_NOTHING, db_column='no_resep', primary_key=True)
    kode_obat = models.ForeignKey('Obat', models.DO_NOTHING, db_column='kode_obat')
    dosis = models.TextField()
    aturan = models.TextField()

    class Meta:
        managed = False
        db_table = 'daftar_obat'
        unique_together = (('no_resep', 'kode_obat'),)


class DaftarTindakan(models.Model):
    id_konsultasi = models.OneToOneField('Tindakan', models.DO_NOTHING, db_column='id_konsultasi', primary_key=True)
    no_urut = models.CharField(max_length=50)
    id_tindakan_poli = models.ForeignKey('TindakanPoli', models.DO_NOTHING, db_column='id_tindakan_poli')

    class Meta:
        managed = False
        db_table = 'daftar_tindakan'
        unique_together = (('id_konsultasi', 'no_urut', 'id_tindakan_poli'),)


class Dokter(models.Model):
    id_dokter = models.CharField(primary_key=True, max_length=50)
    username = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='username')
    no_sip = models.CharField(max_length=50)
    spesialisasi = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'dokter'


class DokterRsCabang(models.Model):
    id_dokter = models.OneToOneField(Dokter, models.DO_NOTHING, db_column='id_dokter', primary_key=True)
    kode_rs = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs')

    class Meta:
        managed = False
        db_table = 'dokter_rs_cabang'
        unique_together = (('id_dokter', 'kode_rs'),)


class JadwalLayananPenunjang(models.Model):
    id_jadwal_penunjang = models.CharField(primary_key=True, max_length=50)
    id_penunjang = models.ForeignKey('LayananPenunjang', models.DO_NOTHING, db_column='id_penunjang')
    hari = models.CharField(max_length=50)
    jam_mulai = models.TimeField()
    jam_selesai = models.TimeField()

    class Meta:
        managed = False
        db_table = 'jadwal_layanan_penunjang'


class JadwalLayananPoliklinik(models.Model):
    id_jadwal_poliklinik = models.CharField(primary_key=True, max_length=50)
    waktu_mulai = models.TimeField()
    waktu_selesai = models.TimeField()
    hari = models.CharField(max_length=50)
    kapasitas = models.IntegerField()
    id_dokter = models.ForeignKey(Dokter, models.DO_NOTHING, db_column='id_dokter')
    id_poliklinik = models.ForeignKey('LayananPoliklinik', models.DO_NOTHING, db_column='id_poliklinik')

    class Meta:
        managed = False
        db_table = 'jadwal_layanan_poliklinik'


class LayananPenunjang(models.Model):
    id_penunjang = models.CharField(primary_key=True, max_length=50)
    kode_rs_cabang = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs_cabang')
    nama = models.CharField(max_length=50)
    tarif = models.IntegerField()
    kategori = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layanan_penunjang'


class LayananPoliklinik(models.Model):
    id_poliklinik = models.CharField(primary_key=True, max_length=50)
    kode_rs_cabang = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs_cabang')
    nama = models.CharField(max_length=50)
    deskripsi = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'layanan_poliklinik'


class NoTelponRs(models.Model):
    kode_rs = models.ForeignKey('RsCabang', models.DO_NOTHING, db_column='kode_rs')
    no_telepon = models.CharField(primary_key=True, max_length=20)

    class Meta:
        managed = False
        db_table = 'no_telpon_rs'
        unique_together = (('no_telepon', 'kode_rs'),)


class Obat(models.Model):
    kode = models.CharField(primary_key=True, max_length=50)
    stok = models.IntegerField()
    harga = models.IntegerField()
    komposisi = models.TextField(blank=True, null=True)
    bentuk_sediaan = models.CharField(max_length=50)
    merk_dagang = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'obat'


class Pasien(models.Model):
    no_rekam_medis = models.CharField(primary_key=True, max_length=50)
    username = models.ForeignKey('Pengguna', models.DO_NOTHING, db_column='username')
    nama_asuransi = models.ForeignKey(Asuransi, models.DO_NOTHING, db_column='nama_asuransi')

    class Meta:
        managed = False
        db_table = 'pasien'


class Pengguna(models.Model):
    email = models.CharField(unique=True, max_length=50)
    username = models.CharField(primary_key=True, max_length=50)
    password = models.TextField()
    nama_lengkap = models.CharField(max_length=50)
    nomor_id = models.CharField(unique=True, max_length=50)
    tanggal_lahir = models.DateField(blank=True, null=True)
    alamat = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'pengguna'


class Resep(models.Model):
    no_resep = models.CharField(primary_key=True, max_length=50)
    id_konsultasi = models.ForeignKey('SesiKonsultasi', models.DO_NOTHING, db_column='id_konsultasi')
    total_harga = models.IntegerField()
    id_transaksi = models.ForeignKey('Transaksi', models.DO_NOTHING, db_column='id_transaksi')

    class Meta:
        managed = False
        db_table = 'resep'


class RsCabang(models.Model):
    kode_rs = models.CharField(primary_key=True, max_length=50)
    nama = models.CharField(max_length=50)
    tanggal_pendirian = models.DateField()
    jalan = models.TextField(blank=True, null=True)
    nomor = models.IntegerField(blank=True, null=True)
    kota = models.TextField()

    class Meta:
        managed = False
        db_table = 'rs_cabang'


class SesiKonsultasi(models.Model):
    id_konsultasi = models.CharField(primary_key=True, max_length=50)
    no_rekam_medis_pasien = models.ForeignKey(Pasien, models.DO_NOTHING, db_column='no_rekam_medis_pasien')
    tanggal = models.DateField()
    biaya = models.IntegerField()
    status = models.CharField(max_length=50)
    id_transaksi = models.ForeignKey('Transaksi', models.DO_NOTHING, db_column='id_transaksi')

    class Meta:
        managed = False
        db_table = 'sesi_konsultasi'


class SesiPemeriksaanPenunjang(models.Model):
    id_pemeriksaan_penunjang = models.CharField(primary_key=True, max_length=50)
    id_konsultasi = models.ForeignKey(SesiKonsultasi, models.DO_NOTHING, db_column='id_konsultasi')
    tanggal = models.DateField()
    id_jadwal_penunjang = models.ForeignKey(JadwalLayananPenunjang, models.DO_NOTHING, db_column='id_jadwal_penunjang')
    id_transaksi = models.ForeignKey('Transaksi', models.DO_NOTHING, db_column='id_transaksi')
    biaya = models.IntegerField()
    status = models.CharField(max_length=50)

    class Meta:
        managed = False
        db_table = 'sesi_pemeriksaan_penunjang'


class Tindakan(models.Model):
    id_konsultasi = models.OneToOneField(SesiKonsultasi, models.DO_NOTHING, db_column='id_konsultasi', primary_key=True)
    no_urut = models.CharField(max_length=50)
    biaya = models.IntegerField()
    catatan = models.TextField(blank=True, null=True)
    id_transaksi = models.ForeignKey('Transaksi', models.DO_NOTHING, db_column='id_transaksi')

    class Meta:
        managed = False
        db_table = 'tindakan'
        unique_together = (('id_konsultasi', 'no_urut'),)


class TindakanPoli(models.Model):
    id_tindakan_poli = models.CharField(primary_key=True, max_length=50)
    id_poliklinik = models.ForeignKey(LayananPoliklinik, models.DO_NOTHING, db_column='id_poliklinik')
    nama_tindakan = models.CharField(max_length=50)
    tarif = models.IntegerField()
    deskripsi = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tindakan_poli'


class Transaksi(models.Model):
    id_transaksi = models.CharField(primary_key=True, max_length=50)
    tanggal = models.DateField()
    status = models.CharField(max_length=50)
    total_biaya = models.IntegerField()
    waktu_pembayaran = models.DateTimeField()
    no_rekam_medis = models.ForeignKey(Pasien, models.DO_NOTHING, db_column='no_rekam_medis')

    class Meta:
        managed = False
        db_table = 'transaksi'
