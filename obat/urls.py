from django.urls import path
from . import views

urlpatterns = [
	path('daftar_obat/',views.daftar_obat,name="daftar_obat"),
	path('update_obat/',views.update_obat,name="update_obat"),
	path('create_obat/',views.create_obat,name="create_obat"),
    path('daftar_obat/remove_obat/', views.remove_obat,name="remove_obat"),
    path('daftar_obat/update_obat/',views.updating,name="updating"),
]