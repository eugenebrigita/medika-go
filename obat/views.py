from django.shortcuts import render, redirect
from .forms import *
from .models import *
# Create your views here.

def daftar_obat(request):
    obats = Obat.objects.order_by("kode")
    response = {'daftar_obat': obats }
    return render(request, 'obat/daftar_obat.html', response)

def update_obat(request):
    form = UpdateObatForm()
    if request.method == "POST":
        id = request.POST['kode']
        obatUpdate=Obat.objects.get(kode=id)
        form = UpdateObatForm(initial={'kode':obatUpdate.kode, 'stok':obatUpdate.stok, 'harga':obatUpdate.harga,
        'komposisi':obatUpdate.komposisi,'bentuk_sediaan':obatUpdate.bentuk_sediaan,'merk_dagang':obatUpdate.merk_dagang})
        return render(request,'obat/update_obat.html', {'form':form})
    args = {'form': form}
    return render(request, 'obat/update_obat.html', args)

def updating(request):
    if request.method == "POST":
        form=UpdateObatForm(request.POST)
        Obat.objects.filter(kode=request.POST.get('kode')).update(stok=request.POST.get('stok'),harga=request.POST.get('harga'),
        komposisi=request.POST.get('komposisi'),bentuk_sediaan=request.POST.get('bentuk_sediaan'),merk_dagang=request.POST.get('merk_dagang'))
    return redirect('daftar_obat')

def create_obat(request):
    form = CreateObatForm()
    if request.method =="POST":
        form=CreateObatForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('daftar_obat')
        else:
            return redirect('create_obat')
    args = {'form': form}
    return render(request, 'obat/create_obat.html', args)

def remove_obat(request):
    if request.method == "POST":
        id = request.POST['id']
        Obat.objects.get(kode=id).delete()
    return redirect('daftar_obat')

