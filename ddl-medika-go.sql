create schema medikago;

set search_path to medikago;

create table pengguna(
  email varchar(50) Not Null Unique,
  username varchar(50) Primary key,
  password text Not Null,
  nama_lengkap varchar(50) Not Null,
  nomor_id varchar(50) Not Null Unique,
  tanggal_lahir date,
  alamat text
);

create table rs_cabang(
  kode_rs varchar(50) Primary key,
  nama varchar(50) Not Null,
  tanggal_pendirian date Not Null,
  jalan text,
  nomor integer,
  kota text Not Null
);

create table administrator(
  nomor_pegawai varchar(50) Primary key,
  username varchar(50) Not Null,
  kode_rs varchar(50) Not Null,
  foreign key (username) references PENGGUNA(username),
  foreign key (kode_rs) references RS_CABANG(kode_rs)
);

create table dokter(
  id_dokter varchar(50) Primary key,
  username varchar(50) Not Null,
  no_sip varchar(50) Not Null,
  spesialisasi text,
  foreign key (username) references PENGGUNA(username)
);

create table LAYANAN_POLIKLINIK(
  id_poliklinik varchar(50) Primary key,
  kode_rs_cabang varchar(50) Not Null,
  nama varchar(50) Not Null,
  deskripsi text,
  foreign key (kode_rs_cabang) references RS_CABANG(kode_rs)
);

create table jadwal_layanan_poliklinik(
  id_jadwal_poliklinik varchar(50) Primary key,
  waktu_mulai time Not Null,
  waktu_selesai time Not Null,
  hari varchar(50) Not Null,
  kapasitas integer Not Null,
  id_dokter varchar(50) Not Null,
  id_poliklinik varchar(50) Not Null,
  foreign key (id_dokter) references DOKTER(id_dokter),
  foreign key (id_poliklinik) references LAYANAN_POLIKLINIK(id_poliklinik)
);

create table dokter_rs_cabang(
  id_dokter varchar(50),
  kode_rs varchar(50),
  Primary key(id_dokter, kode_rs),
  foreign key (id_dokter) references DOKTER(id_dokter),
  foreign key (kode_rs) references RS_CABANG(kode_rs)
);

create table asuransi(
  nama varchar(50) Primary key,
  nomor_telepon varchar(20) Not Null
);

create table asuransi_rs_cabang(
  nama_asuransi varchar(50),
  kode_rs varchar(50),
  Primary key(nama_asuransi, kode_rs),
  foreign key (nama_asuransi) references ASURANSI(nama),
  foreign key (kode_rs) references RS_CABANG(kode_rs)
);

create table no_telpon_rs(
  kode_rs varchar(50),
  no_telepon varchar(20),
  Primary key (no_telepon, kode_rs),
  foreign key (kode_rs) references RS_CABANG(kode_rs)
);

create table layanan_penunjang(
  id_penunjang varchar(50) Primary key,
  kode_rs_cabang varchar(50) Not Null,
  nama varchar(50) Not Null,
  tarif integer Not Null,
  kategori varchar(50),
  foreign key (kode_rs_cabang) references RS_CABANG(kode_rs)
);

create table jadwal_layanan_penunjang(
  id_jadwal_penunjang varchar(50) primary key,
  id_penunjang varchar(50) Not Null,
  hari varchar(50) Not Null,
  jam_mulai time Not Null,
  jam_selesai time Not Null,
  foreign key (id_penunjang) references LAYANAN_PENUNJANG(id_penunjang)
);

create table tindakan_poli(
  id_tindakan_poli varchar(50) Primary key,
  id_poliklinik varchar(50) Not Null,
  nama_tindakan varchar(50) Not Null,
  tarif integer Not Null,
  deskripsi text,
  foreign key (id_poliklinik) references LAYANAN_POLIKLINIK(id_poliklinik)
);

create table pasien(
  no_rekam_medis varchar(50) primary key,
  username varchar(50) not null,
  nama_asuransi varchar(50) not null,
  foreign key (username) references PENGGUNA(username),
  foreign key (nama_asuransi) references ASURANSI(nama)
);

create table  alergi_pasien(
  no_rekam_medis varchar(50),
  alergi varchar(50),
  primary key (no_rekam_medis, alergi),
  foreign key (no_rekam_medis) references PASIEN(no_rekam_medis)
);

create table transaksi(
  id_transaksi varchar(50) primary key,
  tanggal date not null,
  status varchar(50) not null,
  total_biaya integer not null,
  waktu_pembayaran timestamp not null,
  no_rekam_medis varchar(50) not null,
  foreign key (no_rekam_medis) references PASIEN(no_rekam_medis)
);

create table sesi_konsultasi(
  id_konsultasi varchar(50) primary key,
  no_rekam_medis_pasien varchar(50) not null,
  tanggal date not null,
  biaya integer not null,
  status varchar(50) not null,
  id_transaksi varchar(50) not null,
  foreign key (no_rekam_medis_pasien) references PASIEN(no_rekam_medis),
  foreign key (id_transaksi) references TRANSAKSI(id_transaksi)
);

create table sesi_pemeriksaan_penunjang(
  id_pemeriksaan_penunjang varchar(50) primary key,
  id_konsultasi varchar(50) not null,
  tanggal date not null,
  id_jadwal_penunjang varchar(50) not null,
  id_transaksi varchar(50) not null,
  biaya integer not null,
  status varchar(50) not null,
  foreign key (id_konsultasi) references SESI_KONSULTASI(id_konsultasi),
  foreign key (id_jadwal_penunjang) references JADWAL_LAYANAN_PENUNJANG(id_jadwal_penunjang),
  foreign key (id_transaksi) references TRANSAKSI(id_transaksi)
);

create table resep(
  no_resep varchar(50) primary key,
  id_konsultasi varchar(50) not null,
  total_harga integer not null,
  id_transaksi varchar(50) not null,
  foreign key (id_konsultasi) references SESI_KONSULTASI(id_konsultasi),
  foreign key (id_transaksi) references TRANSAKSI(id_transaksi)
);

create table obat(
  kode varchar(50) primary key,
  stok integer not null,
  harga integer not null,
  komposisi text,
  bentuk_sediaan varchar(50) not null,
  merk_dagang varchar(50) not null
);

create table daftar_obat(
  no_resep varchar(50),
  kode_obat varchar(50),
  dosis text not null,
  aturan text not null,
  primary key (no_resep, kode_obat),
  foreign key (no_resep) references RESEP(no_resep),
  foreign key (kode_obat) references OBAT (kode)
);

create table tindakan(
  id_konsultasi varchar(50),
  no_urut varchar(50),
  biaya integer not null,
  catatan text,
  id_transaksi varchar(50) not null,
  primary key (id_konsultasi, no_urut),
  foreign key (id_konsultasi) references SESI_KONSULTASI(id_konsultasi),
  foreign key (id_transaksi) references TRANSAKSI(id_transaksi)
);

create table daftar_tindakan(
  id_konsultasi varchar(50),
  no_urut varchar(50),
  id_tindakan_poli varchar(50),
  primary key (id_konsultasi, no_urut, id_tindakan_poli),
  foreign key (id_konsultasi, no_urut) references TINDAKAN(id_konsultasi, no_urut),
  foreign key (id_tindakan_poli) references TINDAKAN_POLI(id_tindakan_poli)
);
