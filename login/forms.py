from django import forms

class Login_Form(forms.Form):
    username = forms.CharField(label='username', max_length=30, required=True, widget=forms.TextInput(attrs={
        'id':'form',
        'placeholder':'username anda'
    }))

    password = forms.CharField(label='password', max_length=20, required=True, widget=forms.PasswordInput(attrs={
        'id':'form',
        'placeholder':'password anda'
    }))

class Signup_Form_Admin(forms.Form):
    username_attrs={
        'class':'form-control',
        'placeholder': 'username Anda'
    }
    password_attrs={
        'class':'form-control',
        'placeholder': 'password Anda'
    }
    no_identitas={
        'class':'form-control',
        'placeholder': 'Nomor identitas Anda'
    }
    nama_lengkap={
        'class':'form-control',
        'placeholder': 'Nama lengkap Anda'
    }
    tanggal_lahir_attrs={
        'class':'form-control',
        'placeholder': 'MM-DD-YYYY'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    alamat_attrs={
        'id':'form',
        'placeholder': 'alamat'
    }

    username=forms.CharField(
        label='username',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=username_attrs))

    password=forms.CharField(
        label='password',
        required=True,
        max_length=30,
        widget=forms.PasswordInput(attrs=password_attrs))

    nama_lengkap=forms.CharField(
        label='Fullname',
        required=True,
        max_length=50,
        widget=forms.TextInput(attrs=nama_lengkap))

    no_identitas=forms.CharField(
        label='NoIdentitas',
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs=no_identitas))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    tanggal_lahir=forms.CharField(
        label='Tanggal Lahir',
        required=True,
        widget=forms.TextInput(attrs=tanggal_lahir_attrs))

    alamat=forms.CharField(
        label='Alamat',
        required=True,
        widget=forms.TextInput(attrs=alamat_attrs))


class Signup_Form_Pasien(forms.Form):
    username_attrs={
        'class':'form-control',
        'placeholder': 'username Anda'
    }
    password_attrs={
        'class':'form-control',
        'placeholder': 'password Anda'
    }
    no_identitas_attrs={
        'class':'form-control',
        'placeholder': 'Nomor identitas Anda'
    }
    nama_lengkap_attrs={
        'class':'form-control',
        'placeholder': 'Nama lengkap Anda'
    }
    tanggal_lahir_attrs={
        'class':'form-control',
        'placeholder': 'MM-DD-YYYY'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    alamat_attrs={
        'id':'form',
        'placeholder': 'alamat'
    }
    alergi_attrs={
        'id':'form',
        'placeholder': 'alergi'
    }

    username=forms.CharField(
        label='username',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=username_attrs))

    password=forms.CharField(
        label='password',
        required=True,
        max_length=30,
        widget=forms.PasswordInput(attrs=password_attrs))

    nama_lengkap=forms.CharField(
        label='Fullname',
        required=True,
        max_length=50,
        widget=forms.TextInput(attrs=nama_lengkap_attrs))

    no_identitas=forms.CharField(
        label='NoIdentitas',
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs=no_identitas_attrs))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    tanggal_lahir=forms.CharField(
        label='Tanggal Lahir',
        required=True,
        widget=forms.TextInput(attrs=tanggal_lahir_attrs))

    alamat=forms.CharField(
        label='Alamat',
        required=True,
        widget=forms.TextInput(attrs=alamat_attrs))

    alergi=forms.CharField(
        label='alergi',
        required=True,
        widget=forms.TextInput(attrs=alergi_attrs))


class Signup_Form_Dokter(forms.Form):
    username_attrs={
        'class':'form-control',
        'placeholder': 'username Anda'
    }
    password_attrs={
        'class':'form-control',
        'placeholder': 'password Anda'
    }
    no_identitas_attrs={
        'class':'form-control',
        'placeholder': 'Nomor identitas Anda'
    }
    nama_lengkap_attrs={
        'class':'form-control',
        'placeholder': 'Nama lengkap Anda'
    }
    tanggal_lahir_attrs={
        'class':'form-control',
        'placeholder': 'MM-DD-YYYY'
    }
    email_attrs={
        'class':'form-control',
        'placeholder': 'text@example.com'
    }
    alamat_attrs={
        'id':'form',
        'placeholder': 'alamat'
    }
    no_sip_attrs={
        'id':'form',
        'placeholder': 'nomor sip'
    }
    spesialisasi_attrs={
        'id':'form',
        'placeholder': 'spesialisasi'
    }

    username=forms.CharField(
        label='username',
        required=True,
        max_length=30,
        widget=forms.TextInput(attrs=username_attrs))

    password=forms.CharField(
        label='password',
        required=True,
        max_length=30,
        widget=forms.PasswordInput(attrs=password_attrs))

    nama_lengkap=forms.CharField(
        label='Fullname',
        required=True,
        max_length=50,
        widget=forms.TextInput(attrs=nama_lengkap_attrs))

    no_identitas=forms.CharField(
        label='NoIdentitas',
        required=True,
        max_length=20,
        widget=forms.TextInput(attrs=no_identitas_attrs))

    email=forms.EmailField(
        label='Email',
        required=True,
        max_length=30,
        widget=forms.EmailInput(attrs=email_attrs))

    tanggal_lahir=forms.CharField(
        label='Tanggal Lahir',
        required=True,
        widget=forms.TextInput(attrs=tanggal_lahir_attrs))

    alamat=forms.CharField(
        label='Alamat',
        required=True,
        widget=forms.TextInput(attrs=alamat_attrs))

    no_sip=forms.CharField(
        label='No SIP',
        required=True,
        widget=forms.TextInput(attrs=no_sip_attrs))

    spesialisasi=forms.CharField(
        label='spesialisasi',
        required=True,
        widget=forms.TextInput(attrs=spesialisasi_attrs))