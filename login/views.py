from django.shortcuts import render, redirect
from .forms import Signup_Form_Dokter, Signup_Form_Admin, Signup_Form_Pasien, Login_Form
import urllib.request
from django.db import connection

def choice(request):
    return render(request, 'login/choice.html')

def profile(request):
    return render(request, 'login/profile.html')

# def loginpage(request):
# 	forms = Login_Form()
# 	return render(request,'login/login.html', {'forms':forms})

# def signup_admin(request):
# 	response = {}
# 	form = Signup_Form_Admin(request.POST)
# 	response['forms'] = form
# 	return render(request, 'login/signup_admin.html', response)
    # return render(request, 'login/signup_admin.html')

# def signup_pasien(request):
# 	response = {}
# 	form = Signup_Form_Pasien(request.POST)
# 	response['forms'] = form
# 	return render(request, 'login/signup_pasien.html', response)
#     # return render(request, 'login/signup_pasien.html')

# def signup_dokter(request):
# 	response = {}
# 	form = Signup_Form_Dokter(request.POST)
# 	response['forms'] = form
# 	return render(request, 'login/signup_dokter.html', response)

def logout(request):
    request.session.flush()
    return redirect('/')

def loginpage(request):
	response = {}
	form = Login_Form(request.POST)
	response['login'] = form
	if form.is_valid():
		username = form.cleaned_data.get('username')
		password = form.cleaned_data.get('password')

		with connection.cursor() as cursor:
			cursor.execute("set search_path to medikago; select username, password from pengguna where username = '%s' and password='%s'"%(username, password))
			exist = cursor.fetchone()

			if (not exist):
				request.session['none'] = True
			else:
				request.session['user'] = exist
				cursor.execute("set search_path to medikago; select * from pengguna where username='" + username +"';")
				select = cursor.fetchone()
				request.session['username'] = select[1]
				request.session['password'] = select[2]

				cursor.execute("set search_path to medikago; select * from dokter where username='" + username +"';")
				dokter = cursor.fetchone()
				if (dokter):
					request.session['username'] = dokter[1]
					request.session['role'] = 'dokter'
				
				cursor.execute("set search_path to medikago; select * from administrator where username='" + username +"';")
				admin = cursor.fetchone()
				if (admin):
					request.session['username'] = admin[1]
					request.session['role'] = 'admin'

				cursor.execute("set search_path to medikago; select * from pasien where username='" + username +"';")
				pasien = cursor.fetchone()
				if (pasien):
					request.session['username'] = pasien[1]
					request.session['role'] = 'pasien'

				return render(request, 'login/profile.html', response)
			
	return render(request, "login/login.html", response)

def check_email(email):
    with connection.cursor() as cursor:
    	cursor.execute("set search_path to medikago; select email from pengguna where email = '%s'"%(email))
    	mail = cursor.fetchone()
    	if(mail):
    		return True
    	else:
    		return False

def signup_admin(request):
	response = {}
	form = Signup_Form_Admin(request.POST)
	response['forms'] = form
	if request.method == "POST":
		if form.is_valid():
			email = form.cleaned_data["email"]
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			nama_lengkap = form.cleaned_data["nama_lengkap"]
			nomor_id = form.cleaned_data["nomor_id"]
			bdate = form.cleaned_data["bdate"]
			alamat = form.cleaned_data["alamat"]
			# role = "admin"

		with connection.cursor() as cursor:
			if(check_email(email)):
				return render(request, 'login/choice.html', response)
			cursor.execute(
                "insert into pengguna values"
                "(%s,%s,%s,%s,%s,%s,%s);", [email, username, password, nama_lengkap, nomor_id, bdate, alamat])
			cursor.execute(
            	"insert into administrator values"
            	"(%s,%s,%s);", [nomor_id, username, RS01]
            )
		cursor.execute("select * from admin where " + "username = %s AND password= %s", [username, password])
		user = fetchone(cursor)
		request.session['username'] = user[1]
		request.session['role'] = 'admin'

	return render(request, 'login/signup_admin.html', response)

def signup_pasien(request):
	response = {}
	form = Signup_Form_Pasien(request.POST)
	response['forms'] = form
	if request.method == "POST":
		if form.is_valid():
			email = form.cleaned_data["email"]
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			nama_lengkap = form.cleaned_data["nama_lengkap"]
			nomor_id = form.cleaned_data["nomor_id"]
			bdate = form.cleaned_data["bdate"]
			alamat = form.cleaned_data["alamat"]
			alergi = form.cleaned_data["alergi"]
		# role = "pasien"

		with connection.cursor() as cursor:
			if(check_email(email)):
				return render(request, 'login/choice.html', response)
			cursor.execute(
                "insert into pengguna values"
                "(%s,%s,%s,%s,%s,%s,%s);", [email, username, password, nama_lengkap, nomor_id, bdate, alamat,alergi,],
            )

		cursor.execute("select * from pengguna where " + "username = %s AND password= %s", [username, password])
		user = fetchone(cursor)
		request.session['username'] = user[1]
		request.session['role'] = 'pasien'

	return render(request, 'login/signup_pasien.html', response)

def signup_dokter(request):
	response = {}
	form = Signup_Form_Dokter(request.POST)
	response['forms'] = form
	if request.method == "POST":
		if form.is_valid():
			email = form.cleaned_data["email"]
			username = form.cleaned_data["username"]
			password = form.cleaned_data["password"]
			nama_lengkap = form.cleaned_data["nama_lengkap"]
			nomor_id = form.cleaned_data["nomor_id"]
			bdate = form.cleaned_data["bdate"]
			alamat = form.cleaned_data["alamat"]
			no_sip = form.cleaned_data["no_sip"]
			spesialisasi = form.cleaned_data["spesialisasi"]
		# role = "dokter"
		with connection.cursor() as cursor:
			if(check_email(email)):
				return render(request, 'login/choice.html', response)
			cursor.execute(
                "insert into pengguna values"
                "(%s,%s,%s,%s,%s,%s,%s);", [email, username, password, nama_lengkap, nomor_id, bdate, alamat,alergi],)
			cursor.execute(
            	"insert into dokter values"
            	"(%s,%s,%s,%s);", [nomor_id, username, no_sip, spesialisasi],
            )
		cursor.execute("select * from dokter where " + "username = %s AND password= %s", [username, password])
		user = fetchone(cursor)
		request.session['username'] = user[1]
		request.session['role'] = 'dokter'

	return render(request, 'login/signup_dokter.html', response)

