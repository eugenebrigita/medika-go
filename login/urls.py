from django.urls import path
from . import views
# from django.contrib.auth.views import LoginView,LogoutView
urlpatterns = [
	path('loginpage/',views.loginpage,name="loginpage"),
	path('signup_admin/',views.signup_admin,name="signup_admin"),
	path('signup_pasien/',views.signup_pasien,name="signup_pasien"),
	path('signup_dokter/',views.signup_dokter,name="signup_dokter"),
	path('choice/',views.choice,name="choice"),
	path('logout/',views.logout,name="logout"),
	path('profile/',views.profile,name="profile"),
]