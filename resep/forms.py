from django import forms
from django.forms import widgets
from .models import *


class CreateResepForm(forms.Form):
    id_konsultasi=forms.ModelChoiceField(
        queryset=SesiKonsultasi.objects.values_list('id_konsultasi',flat=True),
        empty_label=None
    )
    id_transaksi=forms.ModelChoiceField(
        queryset=Transaksi.objects.values_list('id_transaksi',flat=True),
        empty_label=None
    )
    pick=Obat.objects.values_list('kode',flat=True)
    kode1=forms.ModelChoiceField(
        queryset=pick,
        empty_label=None
    )
    kode2=forms.ModelChoiceField(
        queryset=pick,
        required=False,
        empty_label="-"
    )
    kode3=forms.ModelChoiceField(
        queryset=pick,
        required=False,
        empty_label="-"
    )
    kode4=forms.ModelChoiceField(
        queryset=pick,
        required=False,
        empty_label="-"
    )
