from django.shortcuts import render, redirect
from .forms import *
from .models import *
# Create your views here.

def daftar_resep(request):
    reseps = Resep.objects.order_by("no_resep")
    response = {'daftar_resep': reseps }
    return render(request, 'resep/daftar_resep.html', response)


def create_resep(request):
    form = CreateResepForm()
    if request.method =="POST":
        form=CreateResepForm(request.POST)
        form.save()
        return redirect('daftar_resep')
    args = {'form': form}
    return render(request, 'resep/create_resep.html', args)

def remove_resep(request):
    if request.method == "POST":
        id = request.POST['id']
        Obat.objects.get(kode=id).delete()
    return redirect('daftar_resep')

def create_obat(request):
    form = CreateObatForm()
    if request.method =="POST":
        form=CreateObatForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('daftar_obat')
        else:
            return redirect('create_obat')
    args = {'form': form}
    return render(request, 'obat/create_obat.html', args)
