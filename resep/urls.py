from django.urls import path
from . import views

urlpatterns = [
	path('daftar_resep/',views.daftar_resep,name="daftar_resep"),
	#path('update_resep/',views.update_resep,name="update_resep"),
	path('create_resep/',views.create_resep,name="create_resep"),
    path('daftar_resep/remove_resep/', views.remove_resep,name="remove_resep"),
]