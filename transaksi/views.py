from django.db import connection
from collections import namedtuple
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
import time
from datetime import datetime

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def daftar_transaksi(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('set search_path to medikago')
    cursor.execute('select * from transaksi')
    
    response['transaksi'] = cursor.fetchall()
    return render(request, 'transaksi/daftar_transaksi.html', response)

def delete_transaksi(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "delete from transaksi where id_transaksi = %s", [id]),
    connection.commit()
    return redirect('daftar_transaksi')
    
def create_transaksi(request):
    response = {}
    if (request.method == 'POST'):
            cursor = connection.cursor()
            cursor.execute(
                "set search_path to medikago;"
                "select MAX(id_transaksi) from transaksi;")
            last_id_transaksi = cursor.fetchone()[0].split('TR')
            new_id_transaksi = 'TR' + str(int(last_id_transaksi[1]) + 1)
            id_transaksi = new_id_transaksi
            status = "Created"
            total_biaya = 0
            current_datetime = time.strftime("%Y-%m-%d %H:%M:%S")
            nomor_rekam_medis = request.POST.get('nomor_rekam_medis_dropdown')
            cursor.execute(
                "insert into transaksi values"
                "(%s,%s,%s,%s,%s,%s);", (id_transaksi, current_datetime, status, total_biaya, current_datetime, nomor_rekam_medis,),
            )
            connection.commit()
            return redirect('daftar_transaksi')

    cursor = connection.cursor()
    cursor.execute(
    "set search_path to medikago;" 
    "select distinct no_rekam_medis_pasien FROM sesi_konsultasi, resep, tindakan;"
    )
    response['data'] = cursor.fetchall()
    connection.commit()
    return render(request,"transaksi/create_transaksi.html",response)

def update_transaksi(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select * from transaksi  where id_transaksi =%s", [id]
        )

    data = namedtuplefetchall(cursor)

    return render(request, 'transaksi/update_transaksi.html', {
        "data":data[0]
    })

def update_information(request, id):
    tanggal = datetime.today().strftime("%Y-%m-%d")
    status = request.POST.get('status')
    waktu_pembayaran = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    id_transaksi = id

    cursor = connection.cursor()
    cursor.execute(
    "set search_path to medikago;"
    "select * from transaksi where id_transaksi = %s", [id]
    )
        
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "update transaksi set tanggal=%s, status=%s, waktu_pembayaran=%s where id_transaksi=%s",
        (tanggal, status, waktu_pembayaran, id_transaksi)
    )
    connection.commit()
    return redirect('daftar_transaksi')