from django.urls import path
from . import views

urlpatterns = [
	path('daftar_transaksi/',views.daftar_transaksi,name="daftar_transaksi"),
	path('create_transaksi/',views.create_transaksi,name="create_transaksi"),
	path('update_transaksi/<str:id>/',views.update_transaksi,name="update_transaksi"),
	path('update_information_transaksi/<str:id>/',views.update_information,name="update_information"),
	path('delete_transaksi/<str:id>/', views.delete_transaksi, name="delete_transaksi"),
]