# Tugas Kelompok Basis Data

## Kelompok 36
Link Website Heroku dapat diakses di [sini](http://medika-go-basdat.herokuapp.com/).

## Anggota Kelompok
Eugene Brigita Lauw | 1806141183 | Ilmu Komputer 2018 <br>
Vikih Fitrianih | 1806191130 | Sistem Informasi 2018 <br>
Wiena Amanda | 1806186591 | Ilmu Komputer 2018 <br>
Willy Sandi Harsono | 1806205786 | Ilmu Komputer 2018

## Trigger dan Fitur yang dikerjakan
Merah: Eugene Brigita Lauw <br>
Kuning: Willy Sandi Harsono <br>
Ungu: Vikih Fitrianih<br>
Biru: Wiena Amanda

