from django.db import connection
from collections import namedtuple
from django.shortcuts import render, redirect


# Create your views here.


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def buat_sesi_konsultasi(request):
    response = {}
    if request.method == 'POST':
        cursor = connection.cursor()
        cursor.execute(
            "set search_path to medikago;"
            "select MAX(id_konsultasi) from sesi_konsultasi;"
        )

        last_id_konsultasi = cursor.fetchone()[0].split('SK')
        new_id_konsultasi = 'SK' + str(int(last_id_konsultasi[1]) + 1)
        id_konsultasi = new_id_konsultasi

        no_rekam_medis_pasien = request.POST.get('no_rekam_medis_dropdown')

        tanggal = request.POST.get('tanggal')

        biaya = 0
        status = "BOOKED"

        id_transaksi = request.POST.get('id_transaksi_dropdown')

        sql_query = "set search_path to medikago;" + "insert into sesi_konsultasi values" + "('%s', '%s', '%s', '%s', '%s', '%s');" % (id_konsultasi, no_rekam_medis_pasien, tanggal, biaya, status, id_transaksi,)

        cursor = connection.cursor()
        cursor.execute(
            sql_query
        )
        connection.commit()

        cursor.close()
        return redirect('/daftar-sesi-konsultasi')

    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select distinct no_rekam_medis_pasien FROM sesi_konsultasi, resep, tindakan;"
    )
    response['data'] = cursor.fetchall()

    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select id_transaksi FROM transaksi;"
    )
    response['data_id_transaksi'] = cursor.fetchall()

    connection.commit()

    cursor.close()
    return render(request, 'buat_sesi_konsultasi.html', response)


def daftar_sesi_konsultasi(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select * from sesi_konsultasi"
    )
    response['sesi_konsultasi'] = cursor.fetchall()

    cursor.close()
    return render(request, 'daftar_sesi_konsultasi.html', response)


def update_information(request, id):
    tanggal = request.POST.get('tanggal')
    status = request.POST.get('status')
    id_konsultasi = id

    sql_query = "set search_path to medikago;" + "update sesi_konsultasi set tanggal= '%s', status= '%s' where id_konsultasi= '%s';" % (tanggal, status, id_konsultasi)
    print('haha')
    print(sql_query)
    cursor = connection.cursor()
    cursor.execute(
        sql_query
    )
    connection.commit()

    cursor.close()
    return redirect('/daftar-sesi-konsultasi')


def update_sesi_konsultasi(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select * from sesi_konsultasi where id_konsultasi = %s", [id]
    )

    data = namedtuplefetchall(cursor)

    cursor.close()
    return render(request, 'update_sesi_konsultasi.html', {
        "data":data[0]
    })


def delete_sesi_konsultasi(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "delete from sesi_konsultasi where id_konsultasi = %s", [id]
    )
    connection.commit()

    cursor.close()
    return redirect('daftar-sesi-konsultasi')