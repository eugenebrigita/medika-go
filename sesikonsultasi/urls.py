from django.urls import path
from . import views

urlpatterns = [
    path('buat-sesi-konsultasi/', views.buat_sesi_konsultasi, name="buat-sesi-konsultasi"),
    path('daftar-sesi-konsultasi/', views.daftar_sesi_konsultasi, name="daftar-sesi-konsultasi"),
    path('update-information-sesi-konsultasi/<str:id>/', views.update_information, name="update-information-sesi-konsultasi"),
    path('update-sesi-konsultasi/<str:id>/', views.update_sesi_konsultasi, name="update-sesi-konsultasi"),
    path('delete-sesi-konsultasi/<str:id>/', views.delete_sesi_konsultasi, name="delete-sesi-konsultasi"),
]