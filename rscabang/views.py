from django.db import connection
from collections import namedtuple
from django.shortcuts import render, redirect

# Create your views here.

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def buat_rs_cabang(request):
    if request.method == "POST":
        cursor = connection.cursor()
        cursor.execute(
            "set search_path to medikago;"
            "select MAX(kode_rs) from rs_cabang;"
        )

        last_kode_rs = cursor.fetchone()[0].split('RS')
        new_kode_rs = 'RS0' + str(int(last_kode_rs[1]) + 1)
        kode_rs = new_kode_rs

        nama = request.POST.get('nama')
        tanggal_pendirian = request.POST.get('tanggal_pendirian')
        jalan = request.POST.get('jalan')
        kota = request.POST.get('kota')
        nomor = request.POST.get('nomor')

        sql_query = "set search_path to medikago;" + "insert into rs_cabang values" + "('%s', '%s', '%s', '%s', '%s', '%s');" % (kode_rs, nama, tanggal_pendirian, jalan, nomor, kota,)

        cursor = connection.cursor()
        cursor.execute(
            sql_query
        )
        connection.commit()

        cursor.close()
        return redirect('/daftar-rs-cabang')

    return render(request, 'buat_rs_cabang.html')


def daftar_rs_cabang(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select * from rs_cabang"
    )
    response['rs_cabang'] = cursor.fetchall()

    cursor.close()
    return render(request, 'daftar_rs_cabang.html', response)


def update_information(request, id):
    kode_rs = id
    nama = request.POST.get('nama')
    tanggal_pendirian = request.POST.get('tanggal_pendirian')
    jalan = request.POST.get('jalan')
    kota = request.POST.get('kota')
    nomor = request.POST.get('nomor')

    sql_query = "set search_path to medikago;" + "update rs_cabang set nama= '%s', tanggal_pendirian= '%s', jalan= '%s', nomor= '%s', kota= '%s' where kode_rs= '%s';" % (nama, tanggal_pendirian, jalan, nomor, kota, kode_rs,)

    cursor = connection.cursor()
    cursor.execute(
        sql_query
    )
    connection.commit()

    cursor.close()
    return redirect('daftar-rs-cabang')

def update_rs_cabang(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "select * from rs_cabang where kode_rs = %s", [id]
    )

    data = namedtuplefetchall(cursor)

    cursor.close()
    return render(request, 'update_rs_cabang.html', {
        "data":data[0]
    })


def delete_rs_cabang(request, id):
    cursor = connection.cursor()
    cursor.execute(
        "set search_path to medikago;"
        "delete from rs_cabang where kode_rs = %s", [id]
    )
    connection.commit()

    cursor.close()
    return redirect('daftar-rs-cabang')