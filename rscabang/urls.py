from django.urls import path
from . import views

urlpatterns = [
    path('buat-rs-cabang/', views.buat_rs_cabang, name="buat-rs-cabang"),
    path('daftar-rs-cabang/', views.daftar_rs_cabang, name="daftar-rs-cabang"),
    path('update-information-rs-cabang/<str:id>/', views.update_information, name="update-information-rs-cabang"),
    path('update-rs-cabang/<str:id>/', views.update_rs_cabang, name="update-rs-cabang"),
    path('delete-rs-cabang/<str:id>/', views.delete_rs_cabang, name="delete-rs-cabang")
]
